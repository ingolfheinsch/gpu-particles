//define particle properties
#include "particle_struct.fxh"

float force;
float3 attPos;
float size;
StructuredBuffer<float4> AttractorPosition;
// buffer which holds particle objects
RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 index : SV_DispatchThreadID )
{
	float dist = distance(Output[index.x].pos, attPos);
//	float ga = force/(1.00001-force);
	
	if (dist <=  size*.5)
	{
			float f= abs((dist/size)- 1);
			f=pow(f,(f+1));
			Output[index.x].acc += ((attPos - Output[index.x].pos) * f) * (force*.01);//lerp(0.0f,1.00f,f);
	}
	  	
	  
}

technique10 Emitter
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




