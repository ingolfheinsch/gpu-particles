//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

int pCount;
float	Gamma;	
float radius;
float RepulseAmount;
//define particle properties
#include "particle_struct.fxh"


RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	
	for (uint i=0; i < uint(pCount) ; i++)
	{
		
		float dist = 0;
		float g = Gamma/(1.00001-Gamma);
		dist = distance(Output[DTid.x].pos, Output[i].pos);
		
		if (dist < radius)
		{
	  		float f=saturate(1-dist*2);
	  		f=pow(f,g);
			Output[DTid.x].acc += (Output[DTid.x].pos-Output[i].pos)*lerp(0.0f,1.00f,f)*radius*(RepulseAmount*Output[DTid.x].mass);	
			
		}

	}
	
	
	
	
	
		
		//	Output[DTid.x].vel.y *= (friction/(mass));
		//	Output[DTid.x].pos.y = radius/2;

}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




