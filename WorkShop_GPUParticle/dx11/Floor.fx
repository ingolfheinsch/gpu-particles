//define particle properties
#include "particle_struct.fxh"

float y;
float width;
float BounceFactor = 1;
// buffer which holds particle objects
RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 index : SV_DispatchThreadID )
{
	
	if (Output[index.x].pos.y < y)
	{
		//Output[index.x].vel.y *= -1;
		Output[index.x].pos.y = y;
		Output[index.x].vel.y *= -1 * BounceFactor;
		Output[index.x].acc = 0;
	}
	
/*
	
	if (Output[index.x].pos.x > width)
	{
		Output[index.x].pos.x = width;
		Output[index.x].vel.x *= -1; 
	}
	else if (Output[index.x].pos.x < -width)
	  {
	  	Output[index.x].vel.x *= -1;
	  	Output[index.x].pos.x = -width;
	  	
	  }
	  */
	  	
	  
}

technique10 Emitter
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




