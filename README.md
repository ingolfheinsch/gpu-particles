# GPU Particle Systems with DirectCompute and VVVV #

This repository includes the workshop-material of workshop named "GPU-Particles" which i held together with Paul Scheydt at NODE - Forum for Digital Arts, Frankfurt 2015.

It is basically a collection of DirectCompute Shaders and corresponding vvvv patches.