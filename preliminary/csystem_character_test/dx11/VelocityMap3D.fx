//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 
Texture3D tex <string uiname="Texture";>;

SamplerState volumeSampler // : IMMUTABLE
{
	Filter  =MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
	Addressw = Wrap;
};

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;
//Buffer containing uvs for sampling
 StructuredBuffer<float2> uv <string uiname="UV Buffer";>;
float scale=1;
float fieldPower =1;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(8, 8, 8)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
		//float3 p = Output[DTid.x].pos;
		 float3 p = Output[DTid.x, DTid.y, DTid.z].pos;
 		float3 tempP = p.xyz*scale;
		//col = texVOL.Sample(g_samPoint,float3(In.TexCd.xyz));
		
		float4 c =  tex.SampleLevel(volumeSampler,((tempP+1)/2),0);
		//float4 c = tex.Sample(g_samLinear,((tempP+1)/2),0);
		p =(c.xyz)*fieldPower;
	
		Output[DTid.x].acc += p* (.005*Output[DTid.x].mrl.x);
	
}



technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




