//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

RWStructuredBuffer<float> rwbuffer : BACKBUFFER;
StructuredBuffer<float> uv <string uiname="UV Buffer";>;

Texture2D texture2d <string uiname="Texture";>;

float4x4 tWV: WORLDVIEW ;
float4x4 tWVP: WORLDVIEWPROJECTION ;

float tessfactor <string uiname="Tesselation Factor";> = 1.0f;

static const int MAXMAT = 60;
float4x4 SkinningMatrices[MAXMAT];

SamplerState g_samLinear <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

 
cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
	float4x4 tColor <string uiname="Color Transform";>;
;
};

struct VS_IN_Def
{
	float4 PosO : POSITION;
	float3 Normal			: NORMAL;
	float4 BlendWeights		: BLENDWEIGHT;
	float4 BlendIndices		: BLENDINDICES;
	float4 TexCd: TEXCOORD0;
};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;
    float4 TexCd: TEXCOORD0;
	float3 Normal			: NORMAL;
	float4 BlendWeights		: BLENDWEIGHT;
	float4 BlendIndices		: BLENDINDICES;
};

struct HS_CONSTANT_OUTPUT
{
    float edges[3]        : SV_TessFactor;
    float inside[1]       : SV_InsideTessFactor;
};

struct HS_OUTPUT
{
    float3 cpoint : CPOINT;
	float3 norm : NORMAL;
	 float4 TexCd: TEXCOORD0;
};

struct DS_OUTPUT
{
    float4 position : SV_Position;
	//float3 normV : TEXCOORD0;
	//float3 normV : Normal;
	float4 TexCd: TEXCOORD0;
};

HS_CONSTANT_OUTPUT HSConst(InputPatch<vs2ps, 3> patch)
{
    HS_CONSTANT_OUTPUT output;
    output.edges[0] = tessfactor;
    output.edges[1] = tessfactor;
	output.edges[2] = tessfactor;
	output.inside[0] = tessfactor;
    return output;
}



[domain("tri")] //Indicates we tesselate triangles
[partitioning("fractional_even")]
[outputtopology("triangle_cw")] 

[outputcontrolpoints(3)]
[patchconstantfunc("HSConst")] 
HS_OUTPUT HS(InputPatch<vs2ps, 3> ip, uint id : SV_OutputControlPointID)
{
	//Here we just pass trough the input patch control points,
	//We could modify it here, but it is not needed in our case
    HS_OUTPUT output;
    output.cpoint = ip[id].PosWVP.xyz;
	output.TexCd = ip[id].TexCd;
	output.norm = ip[id].Normal;
    return output;
}

[domain("tri")]
DS_OUTPUT DS(HS_CONSTANT_OUTPUT input, OutputPatch<HS_OUTPUT, 3> op, float3 uv : SV_DomainLocation)
{

    DS_OUTPUT output;
	float2 texCoords = uv.x * op[0].TexCd + uv.y * op[1].TexCd + uv.z * op[2].TexCd;
	//Compute position from barycentric coordinates
	float3 p = uv.x * op[0].cpoint 
        + uv.y * op[1].cpoint 
        + uv.z * op[2].cpoint;
	
	float3 n= uv.x * op[0].norm
        + uv.y * op[1].norm
        + uv.z * op[2].norm;
	// float vHeight = texture2d.Sample(g_samLinear, texCoords) ;
	float vHeight = texture2d.SampleLevel(g_samLinear, texCoords, 0);
n = normalize(n);
	//if (vHeight > .5)
	// p.xyz = vHeight;
	 p.xyz += 1 * (vHeight * 0.2f);
	
	
//to morph our object to sphere, we simply need to normalize position
	

	
	//Add a progressive morphing between box and sphere
	//p = lerp(p,p2,lrp);

	//Transform normals in view space, and position in screen space
	//output.normV = mul(float4(n,0.0f),tWV);	
    //output.position =  mul(float4(p.xyz,1), tWVP);
	// output.position =  mul(p, tWVP);
	 output.position =  float4(p.xyz,1);
    return output;
}



vs2ps VS(VS_IN_Def input)
{
    vs2ps Out = (vs2ps)0;

	float4 blendWeights = input.BlendWeights;
	blendWeights.w = 1.0f - dot( blendWeights.xyz, float3( 1, 1, 1));
	int4 indices = input.BlendIndices;
	
	float4 pos =0;
	float3 norm = input.Normal;

	pos = pos + mul(input.PosO,SkinningMatrices[indices.x]) * blendWeights.x;
 	pos = pos + mul(input.PosO,SkinningMatrices[indices.y]) * blendWeights.y;
	pos = pos + mul(input.PosO,SkinningMatrices[indices.z]) * blendWeights.z;
    pos = pos + mul(input.PosO,SkinningMatrices[indices.w]) * blendWeights.w;

  	norm = norm + mul(input.Normal,SkinningMatrices[indices.x]) * blendWeights.x;
    norm = norm + mul(input.Normal,SkinningMatrices[indices.y]) * blendWeights.y;
	norm = norm + mul(input.Normal,SkinningMatrices[indices.z]) * blendWeights.z;
    norm = norm + mul(input.Normal,SkinningMatrices[indices.w]) * blendWeights.w;
	norm = normalize(norm);

    //position (projected)
    Out.PosWVP  = mul(pos, tWVP);
	//Out.Normal = norm;
 	Out.TexCd = mul(input.TexCd, tTex);
    return Out;
}
/*
float4 PS(vs2ps In): SV_Target
{
	return float4(1,1,1,1);
}
*/
float4 PS(DS_OUTPUT In): SV_Target
{
		return float4(1,1,1,1);
}


technique11 tess
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_5_0, VS() ) );
		SetHullShader( CompileShader( hs_5_0, HS() ) );
		SetDomainShader( CompileShader( ds_5_0, DS() ) );
		SetPixelShader( CompileShader( ps_5_0, PS() ) );
	}

}




