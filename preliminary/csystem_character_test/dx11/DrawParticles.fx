//@author: milo
//@help: particle self collision
//@tags: particule
//@credits: dottore,vux

cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;
};


cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	
}


ByteAddressBuffer sobuffer;
float4x4 tV : VIEW;

float4x4 tVI : VIEWINVERSE;

Texture2D texture2d;

float checkAge;
float4 c0 <bool color=true;> = 1;
float4 c1 <bool color=true;> = 1;

float AlphaPower;
bool AccelerationAlpha = false;

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};

StructuredBuffer<particle> ppos;

float radius = 0.05f;
 
    float3 g_positions[4]:IMMUTABLE =
    {
        float3( -1, 1, 0 ),
        float3( 1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, -1, 0 ),
    };
    float2 g_texcoords[4]:IMMUTABLE = 
    { 
        float2(0,1), 
        float2(1,1),
        float2(0,0),
        float2(1,0),
    };



SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

struct VS_IN
{
	uint iv : SV_VertexID;
	
};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;	
	float2 TexCd : TEXCOORD0;
	float2 age: TEXCOORD1;
	float2 acc: TEXCOORD2;
	float3 norm : NORMAL;
};

vs2ps VS(VS_IN input)
{
    //inititalize all fields of output struct with 0
    vs2ps Out = (vs2ps)0;
	float3 p = ppos[input.iv].pos;

	float radius = ppos[input.iv].mrl.y;  // apply to size
	float a = ppos[input.iv].mrl.z;

  //  Out.PosWVP  = mul(float4(p,1.0),mul(tW,tVP));
	Out.PosWVP  = float4(p,1.0);
	//mul(input.PosO,mul(tW,tVP));
	Out.age.x = ppos[input.iv].age;
	
	Out.age.y = ppos[input.iv].mrl.z;
	
	Out.acc.x = ppos[input.iv].acc;
	Out.acc.y = ppos[input.iv].acc;
	
				//Stride is 24, as 12 for position and 12 for normals, we can't use StreamOut as structured bufffer,
	//So we use byteaddress
	float norm = asfloat(sobuffer.Load(input.iv * 24 +12));

		
	Out.norm = norm;
	
	
    return Out;
}

[maxvertexcount(4)]
void GS(point vs2ps input[1], inout TriangleStream<vs2ps> SpriteStream)
{
    vs2ps output;
    
    //
    // Emit two new triangles
    //
    for(int i=0; i<4; i++)
    {
        float3 position = g_positions[i]*radius;
        position = mul( position, (float3x3)tVI ) + input[0].PosWVP.xyz;
    	//float3 norm = mul(float3(0,0,-1),(float3x3)tVI );
        output.PosWVP = mul( float4(position,1.0), mul(tW,tVP)) ;
        output.age = input[0].age;
    	output.acc = input[0].acc;
        output.TexCd = g_texcoords[i];	
    	output.norm = input[0].norm;
        SpriteStream.Append(output);
    }
    SpriteStream.RestartStrip();
}


float4 PS_Tex(vs2ps In): SV_Target
{
    float4 col = texture2d.Sample( g_samLinear, In.TexCd);
	col *= c0;
	
	if(AccelerationAlpha)
	  col.a *= lerp (0,1,length((In.acc) * pow(AlphaPower,4)));
	
	if (In.norm.x >0)
		discard;

	
	col.a *= lerp (c0.a,c1.a,(In.age.x/In.age.y));
		
	return col;
}


technique10 Constant
{
	pass P0
	{
		
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetGeometryShader( CompileShader( gs_4_0, GS() ) );
		SetPixelShader(  CompileShader(ps_4_0, PS_Tex() ) );
	}
}




