//@author: milo
//@help: particle self collision
//@tags: particule
//@credits: dottore,vux

float4x4 tV : VIEW;
float4x4 tVI : VIEWINVERSE;
RWTexture2D<float4> Tex;

cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;
};


cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
	float4x4 tColor <string uiname="Color Transform";>;
};

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; 
	float age;
};

StructuredBuffer<particle> ppos;

float radius = 0.05f;
 
    float3 g_positions[4]:IMMUTABLE =
    {
        float3( -1, 1, 0 ),
        float3( 1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, -1, 0 ),
    };
    float2 g_texcoords[4]:IMMUTABLE = 
    { 
        float2(0,1), 
        float2(1,1),
        float2(0,0),
        float2(1,0),
    };



SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

struct VS_IN
{
	float4 PosO : POSITION;
	float4 TexCd : TEXCOORD0;
	uint iv : SV_VertexID;
};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;	
	float2 TexCd : TEXCOORD0;
	float2 age: TEXCOORD1;
};

vs2ps VS(VS_IN input)
{
    //inititalize all fields of output struct with 0
    vs2ps Out = (vs2ps)0;
	float3 p = ppos[input.iv].pos;
	float radius = ppos[input.iv].mrl.y;  // apply to size
	float a = ppos[input.iv].mrl.z;
    Out.PosWVP  = mul(input.PosO,mul(tW,tVP));
	Out.age.x = ppos[input.iv].age;
	Out.age.y = ppos[input.iv].mrl.z;
    return Out;
}

[maxvertexcount(4)]
void GS(point vs2ps input[1], inout TriangleStream<vs2ps> SpriteStream)
{
    vs2ps output;
    
    //
    // Emit two new triangles
    //
    for(int i=0; i<4; i++)
    {
        float3 position = g_positions[i]*radius;
        position = mul( position, (float3x3)tVI ) + input[0].PosWVP.xyz;
    	float3 norm = mul(float3(0,0,-1),(float3x3)tVI );
        output.PosWVP = mul( float4(position,1.0), tVP );
        output.age = input[0].age;
        output.TexCd = g_texcoords[i];	
        SpriteStream.Append(output);
    }
    SpriteStream.RestartStrip();
}


float4 PS_Tex(vs2ps In): SV_Target
{
    float4 col = Tex;
	//.Sample( g_samLinear, In.TexCd);
//	col = 1;
	//float4 col = (1,1,1,1);
   
	return col;
}


technique10 Constant
{
	pass P0
	{
		
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		//SetGeometryShader( CompileShader( gs_4_0, GS() ) );
		SetPixelShader(  CompileShader(ps_4_0, PS_Tex() ) );
	}
}




