//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

int pCount;
float	Gamma;	
float RepulseAmount;
struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};


RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float radius = Output[DTid.x].mrl.y;
	for (uint i=0; i < uint(pCount) ; i++)
	{
		
		float dist = 0;
		float g = Gamma/(1.00001-Gamma);
		dist = distance(Output[DTid.x].pos, Output[i].pos);
		
		if (dist < Output[DTid.x].mrl.y)
		{
	  		float f=saturate(1-dist*2);
	  		f=pow(f,g);
			Output[DTid.x].acc += (Output[DTid.x].pos-Output[i].pos)*lerp(0.0f,1.00f,f)*radius*RepulseAmount;					
			
		}
		// }
		
		//if (DTid.x != i)
		//	Output[DTid.x].acc -= (Output[DTid.x].pos-Output[i].pos) *.0000001;
		
		
		
		
	}
	
	
	
	
	
		
		//	Output[DTid.x].vel.y *= (friction/(mass));
		//	Output[DTid.x].pos.y = radius/2;

}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




