//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 



bool reset;


StructuredBuffer<float3> resetPosTemp;
StructuredBuffer<float3> mrl;

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	if (Output[DTid.x].age <= 15)
	Output[DTid.x].pos = resetPosTemp[DTid.x];
}

technique10 Constant
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




