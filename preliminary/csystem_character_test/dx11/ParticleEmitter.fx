//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 



bool reset;
bool useLast = false;

ByteAddressBuffer sobuffer;

StructuredBuffer<float3> resetPos;
StructuredBuffer<float3> mrl; //mass and radius lifetime
StructuredBuffer<float3> iDir; //initial direction of Strands
StructuredBuffer<float2> lenFalloff; //falloff and length of strands
StructuredBuffer<int> isfixed; 
// stride = 27x4Byte
struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};


RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 vel = float3(0,0,0);
	
	if (reset)
	{
		Output[DTid.x].pos = resetPos[DTid.x];
		Output[DTid.x].tmpPos = resetPos[DTid.x];
		Output[DTid.x].mrl = mrl[DTid.x];
		Output[DTid.x].age = 0;
		Output[DTid.x].vel = float3(0,0,0);
		Output[DTid.x].acc = float3(0,0,0);
		Output[DTid.x].iDir = iDir[DTid.x];
		Output[DTid.x].lf = lenFalloff[DTid.x]/100;
		Output[DTid.x].fixed = isfixed[DTid.x];
		
			//Stride is 24, as 12 for position and 12 for normals, we can't use StreamOut as structured bufffer,
	//So we use byteaddress
	float x = asfloat(sobuffer.Load(DTid.x * 24));
	float y = asfloat(sobuffer.Load(DTid.x * 24 + 4));
	float z = asfloat(sobuffer.Load(DTid.x * 24 + 8));
		
		Output[DTid.x].pos.x =x;	
		Output[DTid.x].pos.y =y;
		Output[DTid.x].pos.z =z;
	}
	else
	{
			
			Output[DTid.x].age ++;
	 		vel = Output[DTid.x].acc;
			
			if (useLast && !reset)
			{
				Output[DTid.x].fixed = isfixed[DTid.x];
				Output[DTid.x].iDir = iDir[DTid.x];
				vel *= Output[DTid.x].d;
				Output[DTid.x].tmpPos += vel;
				Output[DTid.x].pos = Output[DTid.x].tmpPos;
			}
			else
		{
				
		
		
		//	float x = asfloat(sobuffer.Load(DTid.x * 24));
		//	float y = asfloat(sobuffer.Load(DTid.x * 24 + 4));
		//	float z = asfloat(sobuffer.Load(DTid.x * 24 + 8));
		
		//		Output[DTid.x].pos.x =x;	
		//		Output[DTid.x].pos.y =y;
		//		Output[DTid.x].pos.z =z;
		Output[DTid.x].pos +=vel;
		}
		
			Output[DTid.x].vel = vel;
	}
}

technique10 Constant
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




