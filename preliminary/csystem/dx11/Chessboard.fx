//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

Texture2D texture2d <string uiname="Texture";>;
int Horizontal = 8;
int Vertical = 8;
float4 Black: COLOR <String uiname="Black";>  = {0, 0, 0, 1};
float4 White: COLOR <String uiname="White";>  = {1, 1, 1, 1};


SamplerState g_samLinear <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

 
cbuffer cbPerDraw : register( b0 )
{
	float4x4 tVP : VIEWPROJECTION;
};


cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
	float4x4 tColor <string uiname="Color Transform";>;
};

struct VS_IN
{
	float4 PosO : POSITION;
	float4 TexCd : TEXCOORD0;

};

struct vs2ps
{
    float4 PosWVP: SV_POSITION;
    float4 TexCd: TEXCOORD0;
};

vs2ps VS(VS_IN input)
{
   vs2ps Out = (vs2ps)0;
    Out.PosWVP  = mul(input.PosO,mul(tW,tVP));
    Out.TexCd = mul(input.TexCd, tTex);
    return Out;
	
	
}




float4 PS(vs2ps In): SV_Target
{
    //compute size
    float2 stepHV = 1/float2(Horizontal, Vertical);

    //compute black|white
    float2 cHV = abs(In.TexCd) / stepHV;
    cHV = cHV % 2 >= 1;

    //special treatment for texcoords < 0
    int2 b = In.TexCd < 0 ? -1 : 0;
    cHV -= b;

    //xor horizontal and vertical stripes
    bool chess = (cHV.x + cHV.y) % 2;

    //missuse a lerp to decide between color 1 and color 2
	float4 col = lerp(Black, White, chess);
	col.a = Alpha;
    return col;
}





technique10 Constant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}




