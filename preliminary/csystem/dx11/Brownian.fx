//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

float brwIndexShift;
float brwnStrenght;
int pCount;
struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};

StructuredBuffer<float3> rndDir;
RWStructuredBuffer<particle> Output : BACKBUFFER;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 acc = (0,0,0);
	
// Brownian
		uint rndIndex = DTid.x + brwIndexShift;
		rndIndex = rndIndex % pCount;
		float3 brwnForce = rndDir[rndIndex];
		acc += (brwnForce * brwnStrenght) * lerp(1,0,Output[DTid.x].age/Output[DTid.x].mrl.z);
	
	Output[DTid.x].acc += acc;

}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




