//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 



bool reset;

float2 xy;
StructuredBuffer<float3> resetPos;
StructuredBuffer<float3> mrl;

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 vel = float3(0,0,0);
	
	if (reset)
	{
		Output[DTid.x].pos.xy = xy;
		Output[DTid.x].mrl = mrl[DTid.x];
		Output[DTid.x].age = 0;
		Output[DTid.x].vel = float3(0,0,0);
		Output[DTid.x].acc = float3(0,0,0);
	}
	else
	{
		Output[DTid.x].age ++;
	 	vel = Output[DTid.x].acc;
		Output[DTid.x].pos += vel;
		Output[DTid.x].vel = vel;
	}
}

technique10 Constant
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




