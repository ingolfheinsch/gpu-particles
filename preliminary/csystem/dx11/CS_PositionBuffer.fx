//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 
Texture2D tex <string uiname="Texture";>;
int elementcount;
float thresh;

SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = MIRROR;
    AddressV = MIRROR;
};

/*This is the buffer from the renderer
Renderer automatically assigns BACKBUFFER semantic
Please note we mark the buffer as append here */
AppendStructuredBuffer<uint> AppendablePositionBuffer : BACKBUFFER;

//Buffer containing uvs for sampling
 StructuredBuffer<float2> uv <string uiname="UV Buffer";>;

 StructuredBuffer<uint> ind <string uiname="index";>;

//==============================================================================
//COMPUTE SHADER ===============================================================
//================================================R==============================

[numthreads(256, 1, 1)]
void StorePosition( uint3 DTid : SV_DispatchThreadID )
{
	
		//Safeguard if we don't use a multiple
	if (DTid.x >=  elementcount) { return;}
	
		float4 c = tex.SampleLevel(g_samLinear,uv[DTid.x],0);

		if (c.x > thresh){
		AppendablePositionBuffer.Append(DTid.x);	
		}
	
}

[numthreads(128, 1, 1)]
void StorePositionForLoop( uint3 DTid : SV_DispatchThreadID )
{
	
	for (uint i=0; i < uint(elementcount) ; i++)
	{
	
		float4 c = tex.SampleLevel(g_samLinear,uv[i],0);

		if (c.x > thresh){
		AppendablePositionBuffer.Append(i);	
		}
}}


technique11 DispatchedThreaded
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, StorePosition() ) );
	}
}

technique11 ForLoop
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, StorePositionForLoop() ) );
	}
}




