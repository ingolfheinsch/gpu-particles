//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 


//That's now our lookup table
StructuredBuffer<uint> PositionIndexA;
//That's now our lookup table
StructuredBuffer<uint> PositionIndexB;

//Contains structure count
ByteAddressBuffer InputCountBufferA;

//Contains structure count
ByteAddressBuffer InputCountBufferB;


float thresh;

SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = MIRROR;
    AddressV = MIRROR;
};

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
	bool type;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;
//Buffer containing uvs for sampling
 StructuredBuffer<float2> uv <string uiname="UV Buffer";>;
//Buffer containing resetPositions
 StructuredBuffer<float2> Resetpos <string uiname="Reset Buffer";>;

float2 ScaleXY=1;
float fieldPower =1;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
		uint count =0;
		if (Output[DTid.x].type)
			count = InputCountBufferA.Load(0);
		else
			count = InputCountBufferB.Load(0);
	
		float3 currentPos = Output[DTid.x].pos;
		uint myindex = 0;
		if (Output[DTid.x].type)
			myindex = PositionIndexA[DTid.x%count];
		else
			myindex = PositionIndexB[DTid.x%count];
	
		float3 targetpos;
		targetpos.xy = uv[myindex]*ScaleXY;
		targetpos.z=0;
	
		Output[DTid.x].acc.xyz += ((Output[DTid.x].pos -(targetpos*2-1)))*-0.01*fieldPower * Output[DTid.x].mrl.x;
		
}



technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




