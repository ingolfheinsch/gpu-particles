//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

float brwIndexShift;
float brwnStrenght;
int pCount;
struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
};

struct position
{
	float3 pos;
};

StructuredBuffer<float3> rndDir;
RWStructuredBuffer<particle> Output : BACKBUFFER;
RWStructuredBuffer<position> PositionOut : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CopyPostion( uint3 DTid : SV_DispatchThreadID )
{

	PositionOut[DTid.x].pos = Output[DTid.x].pos;

}

technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CopyPostion() ) );
	}
}




