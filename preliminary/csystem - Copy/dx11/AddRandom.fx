//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

float3 influence;
bool doCalc = false;

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};



RWStructuredBuffer<particle> Output : BACKBUFFER;


uint wang_hash(uint seed)
{
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return seed;
}

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 acc = float3(0,0,0);
	float3 vel= float3(0,0,0);
	float3 force = float3(0,0,0);
	
	if (doCalc)
	{
		force.x = float(wang_hash(DTid.x)) * (1.0 / 4294967296.0);
		force.y = float(wang_hash(DTid.x+1)) * (1.0 / 4294967296.0);
		force.z = float(wang_hash(DTid.x+2)) * (1.0 / 4294967296.0);
		acc += (lerp(0,1,force))*influence;
	}
	else
		acc = float3(0,0,0);
	
	Output[DTid.x].acc += acc;
	
}





technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




