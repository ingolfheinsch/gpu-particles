//@author: milo
//@help: Constraint Shader 
//@tags: color
//@credits: Fast Simulation of Inextensible Hair and Fur by M.Mueller

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};

StructuredBuffer<float3> resetPos;
RWStructuredBuffer<particle> Output : BACKBUFFER;
int strandCount;
int strandLenght;
bool init;
float lenght;
bool fixed=true;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	if ((DTid.x%strandLenght == 0 && strandCount> 1 ) && Output[DTid.x].fixed)
		Output[DTid.x].pos = resetPos[DTid.x];

	int id = DTid.x ;
	float3 currPos = Output[id].pos;
	float3 lastPos = Output[id-1].pos;
	float3 dir = normalize(currPos - lastPos);
	
	if (init)
		Output[id].tmpPos += Output[id].lf.x;
	else
		Output[id].tmpPos = ((lastPos + dir * Output[id].lf.x) + Output[id].acc);
	
	Output[id].d = currPos - Output[id].tmpPos;// - curRrPos;
	//Output[id].d = Output[DTid.x].iDir;
}

technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




