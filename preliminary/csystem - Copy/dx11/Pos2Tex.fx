//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 
//
//struct particle
//{
//	float3 pos;
//	float3 vel;
//	float3 acc;
//	float3 mrl; //mass and radius lifetime
//	float age;
//};

//Texture2D<float4> inputTex;
//RWStructuredBuffer<particle> Output : BACKBUFFER;
RWTexture2D<float4> texCopy;
Texture2D<float4> tex;

SamplerState s0 <string uiname="Sampler State";>
{Filter=MIN_MAG_MIP_LINEAR;AddressU=CLAMP;AddressV=CLAMP;};

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(8, 8, 1)]
void CS( uint3 id : SV_DispatchThreadID )
{
	//float3 acc = (0,0,0);
//float2 TexCd = (0.5,.5);
	//TexCd*=0.5*float2(1,-1)+0.5;
	int w = 64;
	int h = 64;
	float2 uv = float2(id.x/w, id.y/h);
 
  //  float4 t = tex.SampleLevel(s0, uv, 0);
 	//float4 t = tex[id.xy];
 	float4 t = (1,1,1,1);
    texCopy[id.xy] = t;
 
    //tex[id.xy] = float4(uv, 0.0, 1.0);
	
	//tex[id.xy] = float3(1.0f, 1.0f, 1.0f);

}



technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CS() ) );
	}
}




