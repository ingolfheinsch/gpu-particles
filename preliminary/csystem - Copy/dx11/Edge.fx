//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 


float2 edge;
float width;
float power;
struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
};

RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 acc = float3(0,0,0);
	float3 vel = Output[DTid.x].vel;
	float mass = Output[DTid.x].mrl.x;
	float radius = Output[DTid.x].mrl.y;
	float friction =-.5;
	
	if ((Output[DTid.x].pos.x + radius/2 > edge.x-width && Output[DTid.x].pos.x + radius/2 < edge.x+width) || (Output[DTid.x].pos.y + radius/2 > edge.y-width && Output[DTid.x].pos.y + radius/2 < edge.y+width))
	{
			
		//	Output[DTid.x].vel.y *= (friction/(mass));
			Output[DTid.x].acc *= -1; 
		//	Output[DTid.x].pos.y = -1+radius/2;
	}
	//else
		//Output[DTid.x].acc = 0; 
	
	

	//acc = float3(0,0,0);
}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




