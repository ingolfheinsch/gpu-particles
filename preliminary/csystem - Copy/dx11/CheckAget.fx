//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};
bool bufferPos = true;
float3 resetPosition;
RWStructuredBuffer<particle> Output : BACKBUFFER;
StructuredBuffer<float3> resetPos;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{

	
	if (Output[DTid.x].age > Output[DTid.x].mrl.z)
	{
		Output[DTid.x].acc =float3 (0,0,0);
		if (bufferPos)
			Output[DTid.x].pos = resetPos[DTid.x];
		else
			Output[DTid.x].pos = resetPosition;
		Output[DTid.x].age = 0;
	}

}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




