//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

//#include "classicnoise2D.fxh"
#include "noise4D.fxh"

float3 force;
bool doCalc = false;

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
};


RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
	float3 acc = Output[DTid.x].acc;
	float3 vel = Output[DTid.x].vel;
	float mass = Output[DTid.x].mrl.x;
	
	if (doCalc)
		acc = ((mass/1000)/2)*force;
	else
		acc = float3(0,0,0);
	
	Output[DTid.x].acc += acc;
	
}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




