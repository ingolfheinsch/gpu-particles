//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 


float viscoyity;

struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};

RWStructuredBuffer<particle> Output : BACKBUFFER;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 DTid : SV_DispatchThreadID )
{
		Output[DTid.x].acc *= (viscoyity);
}



technique10 Constant
{
	pass P0s
	{
	SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




