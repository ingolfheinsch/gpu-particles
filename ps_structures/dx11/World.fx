//define particle properties
#include "particle_struct.fxh"

bool reset;
float friction;
float3 gravity;
// buffer which holds particle objects
RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 index : SV_DispatchThreadID )
{
	if (reset)
	{
		Output[index.x].pos = float3(-1000,0,0);
		Output[index.x].vel = 0;
	}
		//Calc New Velocity from Acceleration
		float3 forces = Output[index.x].acc + gravity*0.001;
		forces *= Output[index.x].mass;
		float3 velocity = forces + Output[index.x].vel*friction;
		//Calc Friction
		
		//Save the new Velocity in Buffer
		Output[index.x].vel = velocity;
		//Apply Velocity to Position
		Output[index.x].pos += velocity;	
	//	Output[index.x].acc = forces;
		Output[index.x].acc = 0;
		Output[index.x].age += 1;
}

technique10 World
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




