//@author: milo
//@help: Constraint Shader 
//@tags: color
//@credits: Fast Simulation of Inextensible Hair and Fur by M.Mueller
//#include "particle_struct.fxh"
#include "particle_constraints_struct.fxh"

float4x4 transform;
/*
struct particle
{
	float3 pos;
	float3 vel;
	float3 acc;
	float3 mrl; //mass and radius lifetime
	float age;
	/// fields used for particle strands
	float3 d; // direction
	float3 tmpPos;// temporary postion for calculating verlet chain
	float3 iDir; // initial direction of the strand
	float2 lf; //length and fallof for direction per particle in strand
	bool fixed;
};
*/
ByteAddressBuffer RAWMehsData_Buffer;
RWStructuredBuffer<particle> Output : BACKBUFFER;
int strandCount;
int strandLenght;
bool init;
float lenght;
bool fixed=true;
int MaxCountMehs;
//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 index : SV_DispatchThreadID )
{
	float x = asfloat(RAWMehsData_Buffer.Load((index.x%MaxCountMehs) * 12));
	float y = asfloat(RAWMehsData_Buffer.Load((index.x%MaxCountMehs) * 12 + 4));
	float z = asfloat(RAWMehsData_Buffer.Load((index.x%MaxCountMehs) * 12 + 8));
	
	float3 p = float3(x,y,z);
	p = mul(float4(p,1), transform).xyz;
	if (index.x%strandLenght == 0 && strandCount> 1 )
		Output[index.x].pos = p;

	int id = index.x ;
	float3 currPos = Output[id].pos;
	float3 lastPos = Output[id-1].pos;
	float3 dir = normalize(currPos - lastPos);
	float3 tmpPos;
	
	if (init)
		Output[id].tmpPos += lenght;
	else
		Output[id].tmpPos = ((lastPos + dir * lenght) + Output[id].acc);
	
	//Output[id].d = currPos - Output[id].tmpPos;
}

technique10 Constant
{
	pass P0s
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




