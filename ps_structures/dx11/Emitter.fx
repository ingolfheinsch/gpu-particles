//define particle properties
#include "particle_struct.fxh"

int indexOffset;
int emitCount;
int particleCount;
bool emit;

// emitter position
StructuredBuffer<float3> EmitterPos;
StructuredBuffer<float3> Acceleration;
// buffer which holds particle objects
RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(1, 1, 1)]
void CSConstantForce( uint3 index : SV_DispatchThreadID )
{
	if (emit)
	{
	// get initial position from Emitter
	uint emitIndex = index.x% emitCount;

	float3 p = EmitterPos[emitIndex];
	float3 a = Acceleration[emitIndex]*.1;
	
	//calc index offset
	uint bufferIndex = indexOffset + index.x;
	bufferIndex %= particleCount;
	
	Output[bufferIndex].acc = a;
	Output[bufferIndex].pos = p;
	}
}

technique10 Emitter
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




