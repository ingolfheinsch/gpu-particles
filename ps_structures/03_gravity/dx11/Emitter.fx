//define particle properties
#include "particle_struct.fxh"

bool reset;
float2 mouseXY;
// emitter position
StructuredBuffer<float3> EmitterPos;
StructuredBuffer<float3> Velocity;
StructuredBuffer<int> Lifetime;
// buffer which holds particle objects
RWStructuredBuffer<particle> Output : BACKBUFFER;

//==============================================================================
//COMPUTE SHADER ===============================================================
//==============================================================================

[numthreads(128, 1, 1)]
void CSConstantForce( uint3 index : SV_DispatchThreadID )
{
	// get initial position from Emitter
	if (reset)	
	{
		Output[index.x].pos = EmitterPos[index.x];
		Output[index.x].vel = Velocity[index.x];
		Output[index.x].age =0;
	}
	
	if (Output[index.x].age > Lifetime[index.x])
	{
		Output[index.x].pos = EmitterPos[index.x] + float3(mouseXY,0);
		Output[index.x].age =0;
		Output[index.x].vel = Velocity[index.x];
	}
	
	//lifecylcle update
	Output[index.x].pos += Output[index.x].vel;
	Output[index.x].age += 1;
	
		
		
}

technique10 Emitter
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSConstantForce() ) );
	}
}




